/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/


var avatarUrlDefault = {
  avatarUrl: 'default1.jpg',
  like: function (){
       this.likes ++;
  },
};

  var Comment = function  (name, text, avatarUrl) {
    this.name = name;
    this.text = text;
      if( avatarUrl !== undefined ){
        this.avatarUrl = avatarUrl;
      };
    this.likes=0;

  console.log('new obj', this);
  Object.setPrototypeOf(this, avatarUrlDefault);
};

var myComments1 = new Comment ('Ivan', 'Speak message1');
var myComments2 = new Comment ('Sveta', 'Speak message2');
var myComments3 = new Comment ('Sasha', 'Speak message3', 'avat.jpg');
var myComments4 = new Comment ('Petya', 'Speak message4', 'avat.jpg');

var CommentsArray = [myComments1, myComments2, myComments3, myComments4];

 function Show (array){
   document.body.innerHTML = null;
   CommentsArray.forEach(function (item){
      var add = document.createElement('div');
      add.innerHTML ='<img src=' + item.avatarUrl + '>'+'<br>'+ item.name+': '+'<br>'+item.text+': ' ;
      document.body.appendChild(add);

      var button = document.createElement('button');

      button.addEventListener('click', function(){
        item.like();
        Show();
      });
      button.innerHTML = "like: " + item.likes;
      add.appendChild(button);

  });
};
Show();
