/*

  Objects

// */
//       var settings = {
//         colorsCount: 20,
//         title: "MyObj2",
//         targetId: "wrap"
//       };
//
//       // var x = Object.create(settings);
//       //     x.myValue = '123';
//       // console.log('ObjectCreate', x, x.__proto__.title);
//
//       function generateMyObject( settings ){
//         this.colorsCount = name;
//
//         function privateMethod(){
//             this.name = "rrr";
//         }
//
//
//         this.publicMethod = function(){
//           console.log('before', this.name);
//           privateMethod.apply(this);
//           console.log('after', this.name);
//         };
//         return this;
//       }
//       //
//       var x = new generateMyObject(settings);
//           x.publicMethod();
//           // console.log(x);
//           // x.privateMethod();
// /



  function Planet (name) {
      this.name = name;
      this.population = randomPopulation();
      var populationMultiplyRate = 3;

      function randomPopulation(){
        return Math.floor(Math.random()*1000);
      };

      function growPopulation(){
        var state = populationMultiplyRate*100+this.population;
        console.log('прибавилось' + state +' населения на планете');
      };
      function Cataclysm(){
        var state = this.population + Math.floor(Math.random(1,10)*1000);
        console.log('от катаклизма погибло'+ state + 'человек на планет');
      };

      this.rotatePlanet = function(){
        let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
        if ( (randomNumber % 2) == 0) {
          growPopulation.apply(this);
        } else {
          Cataclysm.apply(this);
        }
      };
  };

    var Mars = new Planet('Mars');
    console.log(Mars);

    Mars.rotatePlanet();
    

    /*

      Задание:

      Написать функцию генератор, которая будет иметь приватные и публичные свойства.
      Публичные методы должны вызывать приватные.

      Рассмотрим на примере планеты:

        - На вход принимаются параметр Имя планеты.

        Создается новый обьект, который имеет публичные методы и свойства:
        - name (передается через конструктор)
        - population ( randomPopulation());
        - rotatePlanet(){
          let randomNumber = Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
          if ( (randomNumber % 2) == 0) {
            growPopulation(state);
          } else {
            Cataclysm(state);
          }
        }

        Приватные методы
        randomPopulation -> Возвращает случайное целое число от 1.000 до 10.000

        growPopulation() {
          функция которая берет приватное свойство populationMultiplyRate
          которое равняется случайному числу от 1 до 10 и умножает его на 100.
          Дальше, число которое вышло добавляет к популяции и выводит в консоль сообщение,
          что за один цикл прибавилось столько населения на планете .
        }

        Cataclysm(){
          Рандомим число от 1 до 10 и умножаем его на 10000;
          То число которое получили, отнимаем от популяции.
          В консоль выводим сообщение - от катаклизма погибло Х человек на планете.
        }

    */
